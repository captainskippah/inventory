<?php

namespace Captainskippah\Inventory\Infrastructure;

use Captainskippah\Common\Event\EventStore;
use Captainskippah\Inventory\Domain\Product\Product;
use Captainskippah\Inventory\Domain\Product\ProductId;
use Captainskippah\Inventory\Domain\Product\ProductRepository;
use Ramsey\Uuid\Uuid;

class EventStoreProductRepository implements ProductRepository
{
    /**
     * @var EventStore
     */
    private $eventStore;

    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function getById(ProductId $productId): ?Product
    {
        $eventStream = $this->eventStore->loadEventStream($productId);

        if ($eventStream->version() === 0) {
            return null;
        }

        return Product::fromEventStream($eventStream);
    }

    public function save(Product $product)
    {
        $this->eventStore->appendToStream($product->id(), $product->unmutatedVersion() + 1, ...$product->events());

        $product->clearEvents();
    }

    public function delete(ProductId $productId)
    {
        $this->eventStore->deleteStream($productId);
    }

    public function nextIdentity(): ProductId
    {
        return new ProductId(Uuid::uuid4()->serialize());
    }
}
