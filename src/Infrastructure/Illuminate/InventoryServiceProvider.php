<?php

namespace Captainskippah\Inventory\Infrastructure\Illuminate;

use Captainskippah\Common\Domain\Dispatcher;
use Captainskippah\Inventory\Application\SaleCreatedHandler;
use Captainskippah\Inventory\Domain\Product\ProductRepository;
use Captainskippah\Inventory\Domain\Sales\SaleRepository;
use Captainskippah\Inventory\Infrastructure\EventStoreProductRepository;
use Captainskippah\Inventory\Infrastructure\Illuminate\Repositories\DBSaleRepository;
use Illuminate\Support\ServiceProvider;

class InventoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ProductRepository::class, EventStoreProductRepository::class);

        $this->app->bind(SaleRepository::class, function ($app) {
            return new DBSaleRepository($app->make('db.connection'));
        });
    }

    public function boot()
    {
        $this->publishes([__DIR__ . '/migrations' => database_path('migrations')], 'migrations');

        Dispatcher::instance()->register($this->app->make(SaleCreatedHandler::class));
    }
}
