<?php

namespace Captainskippah\Inventory\Infrastructure\Illuminate\Repositories;

use Captainskippah\Common\Domain\Dispatcher;
use Captainskippah\Common\Illuminate\Repository\AbstractDBRepository;
use Captainskippah\Inventory\Domain\Product\ProductId;
use Captainskippah\Inventory\Domain\Sales\Sale;
use Captainskippah\Inventory\Domain\Sales\SaleId;
use Captainskippah\Inventory\Domain\Sales\SaleItem;
use Captainskippah\Inventory\Domain\Sales\SaleRepository;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;
use stdClass;

class DBSaleRepository extends AbstractDBRepository implements SaleRepository
{
    public function getById(SaleId $saleId): ?Sale
    {
        $sale = $this->query()
            ->find($saleId->value());

        if ($sale === null) {
            return null;
        }

        $items = $this->queryItems()
            ->where('sale_id', $saleId->value())
            ->get()
            ->map(function (stdClass $item) {
                return new SaleItem(new ProductId($item->product_id), $item->qty, $item->price);
            })
            ->toArray();

        $reflection = new \ReflectionClass(Sale::class);

        $instance = $reflection->newInstanceWithoutConstructor();

        $propId = $reflection->getProperty('id');
        $propId->setAccessible(true);
        $propId->setValue($instance, $saleId);

        $propDate = $reflection->getProperty('date');
        $propDate->setAccessible(true);
        $propDate->setValue($instance, CarbonImmutable::createFromFormat('Y-m-d', $sale->date));

        $propItems = $reflection->getProperty('items');
        $propItems->setAccessible(true);
        $propItems->setValue($instance, $items);

        return $instance;
    }

    public function save(Sale $sale)
    {
        $this->database->transaction(function () use ($sale) {
            $this->query()
                ->insert([
                    'id' => $sale->id()->value(),
                    'date' => $sale->date()->format('Y-m-d')
                ]);

            $values = [];

            foreach ($sale->items() as $item) {
                $values[] = [
                    'sale_id' => $sale->id()->value(),
                    'product_id' => $item->productId()->value(),
                    'qty' => $item->qty(),
                    'price' => $item->price()
                ];
            }

            $this->queryItems()->insert($values);

            foreach ($sale->events() as $event) {
                Dispatcher::instance()->dispatch($event);
            }

            $sale->clearEvents();
        });
    }

    public function nextIdentity(): SaleId
    {
        return new SaleId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table('inventory_sales');
    }

    private function queryItems()
    {
        return $this->database->table('inventory_sale_items');
    }
}
