<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_sales', function (Blueprint $table) {
            $table->uuid('id');
            $table->date('date');

            $table->primary('id');
        });

        Schema::create('inventory_sale_items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('sale_id');
            $table->uuid('product_id');
            $table->integer('qty');
            $table->decimal('price', 12, 2);

            $table->foreign('sale_id')
                ->references('id')
                ->on('inventory_sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_sale_items');
        Schema::dropIfExists('inventory_sales');
    }
}
