<?php

namespace Captainskippah\Inventory\Application;

use Captainskippah\Common\Domain\DomainEvent;
use Captainskippah\Common\Domain\EventListener;
use Captainskippah\Inventory\Domain\Product\InventoryMethod;
use Captainskippah\Inventory\Domain\Product\ProductRepository;
use Captainskippah\Inventory\Domain\Sales\SaleCreated;

class SaleCreatedHandler extends EventListener
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var InventoryMethod
     */
    private $method;

    public function __construct(ProductRepository $productRepository, InventoryMethod $method)
    {
        $this->productRepository = $productRepository;
        $this->method = $method;
    }

    public static function canHandle(DomainEvent $event): bool
    {
        return $event instanceof SaleCreated;
    }

    protected function doHandle(DomainEvent $event)
    {
        foreach ($event->items() as $item) {
            $product = $this->productRepository->getById($item->productId());
            $product->take($this->method, $item->qty(), $event->date());
            $this->productRepository->save($product);
        }
    }
}
