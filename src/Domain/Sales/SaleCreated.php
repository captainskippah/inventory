<?php

namespace Captainskippah\Inventory\Domain\Sales;

use Captainskippah\Common\Domain\DomainEvent;
use Carbon\CarbonImmutable;

class SaleCreated extends DomainEvent
{
    /**
     * @var SaleId
     */
    private $id;

    /**
     * @var CarbonImmutable
     */
    private $date;

    /**
     * @var SaleItem[]
     */
    private $items = [];

    public function __construct(SaleId $id, CarbonImmutable $date, SaleItem ... $items)
    {
        parent::__construct();
        $this->id = $id;
        $this->date = $date;
        $this->items = $items;
    }

    public function id(): SaleId
    {
        return $this->id;
    }

    public function date(): CarbonImmutable
    {
        return $this->date;
    }

    /**
     * @return SaleItem[]
     */
    public function items(): array
    {
        return $this->items;
    }
}
