<?php

namespace Captainskippah\Inventory\Domain\Sales;

interface SaleRepository
{
    public function getById(SaleId $saleId): ?Sale;

    public function save(Sale $sale);

    public function nextIdentity(): SaleId;
}
