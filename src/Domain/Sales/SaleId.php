<?php

namespace Captainskippah\Inventory\Domain\Sales;

use Captainskippah\Common\Domain\AbstractId;

class SaleId extends AbstractId
{
    public function __construct(string $saleId)
    {
        parent::__construct($saleId);
    }
}
