<?php

namespace Captainskippah\Inventory\Domain\Sales;

use Captainskippah\Inventory\Domain\Product\ProductId;

class SaleItem
{
    /**
     * @var ProductId
     */
    private $productId;

    /**
     * @var int
     */
    private $qty;

    /**
     * @var float
     */
    private $price;

    public function __construct(ProductId $productId, int $qty, float $price)
    {
        $this->productId = $productId;
        $this->qty = $qty;
        $this->price = $price;
    }

    public function productId(): ProductId
    {
        return $this->productId;
    }

    public function qty(): int
    {
        return $this->qty;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function total(): float
    {
        return $this->price() * $this->qty();
    }
}
