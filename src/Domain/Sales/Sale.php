<?php

namespace Captainskippah\Inventory\Domain\Sales;

use Captainskippah\Common\Domain\AggregateRoot;
use Carbon\CarbonImmutable;

class Sale extends AggregateRoot
{
    /**
     * @var SaleId
     */
    private $id;

    /**
     * @var CarbonImmutable
     */
    private $date;

    /**
     * @var SaleItem[]
     */
    private $items = [];

    public function __construct(SaleId $id, CarbonImmutable $date, SaleItem ...$items)
    {
        if (count($items) === 0) {
            throw new \Exception('Sale should have items');
        }

        $this->id = $id;
        $this->date = $date;
        $this->items = $items;
        $this->events[] = new SaleCreated($id, $date, ...$items);
    }

    public function id(): SaleId
    {
        return $this->id;
    }

    public function date(): CarbonImmutable
    {
        return $this->date;
    }

    /**
     * @return SaleItem[]
     */
    public function items(): array
    {
        return $this->items;
    }
}
