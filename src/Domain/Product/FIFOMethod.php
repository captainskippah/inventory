<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;

class FIFOMethod implements InventoryMethod
{
    public function takeNextStock(CarbonImmutable $date, AbstractStock ...$stocks): ?AbstractStock
    {
        $oldest = null;

        foreach ($stocks as $stock) {
            // Cannot take stock from the future
            if ($stock->date()->isAfter($date)) {
                continue;
            }

            if ($stock instanceof MissingStock) {
                continue;
            }

            if ($stock->qty() === 0) {
                continue;
            }

            if ($oldest === null) {
                $oldest = $stock;
                continue;
            }

            if ($stock->date()->isBefore($oldest)) {
                $oldest = $stock;
            }
        }

        return $oldest;
    }
}
