<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;

class LIFOMethod implements InventoryMethod
{
    public function takeNextStock(CarbonImmutable $date, AbstractStock ...$stocks): ?AbstractStock
    {
        $newest = null;

        foreach ($stocks as $stock) {
            // Cannot take stock from the future
            if ($stock->date()->isAfter($date)) {
                continue;
            }

            if ($stock instanceof MissingStock) {
                continue;
            }

            if ($stock->qty() === 0) {
                continue;
            }

            if ($newest === null) {
                $newest = $stock;
                continue;
            }

            if ($stock->date()->isSameDay($newest->date()) || $stock->date()->isAfter($newest->date())) {
                $newest = $stock;
            }
        }

        return $newest;
    }
}
