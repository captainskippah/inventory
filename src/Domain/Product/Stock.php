<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;
use Hamcrest\Thingy;

class Stock extends AbstractStock
{
    /**
     * @var StockId
     */
    private $id;

    /**
     * @var float
     */
    private $cost;

    public function __construct(StockId $id, float $cost, int $qty, CarbonImmutable $date)
    {
        parent::__construct($qty, $date);
        $this->id = $id;
        $this->cost = $cost;
    }

    public function id(): StockId
    {
        return $this->id;
    }

    public function cost(): float
    {
        return $this->cost;
    }

    public function amendCost(float $cost)
    {
        $this->cost = $cost;
    }

    public function increase(int $qty)
    {
        if ($qty <= 0) {
            throw new \RuntimeException('Quantity should be positive. Given: ' . $qty);
        }

        $this->qty += $qty;
    }

    public function decrease(int $qty)
    {
        if ($qty === 0) {
            throw new \RuntimeException(sprintf('Stock of ID %s is at 0', $this->id()->value()));
        }

        if ($qty > $this->qty()) {
            throw new \RuntimeException('Cannot take more than available stock of ' . $this->qty());
        }

        $this->qty -= $qty;
    }
}
