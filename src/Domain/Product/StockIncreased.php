<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;
use Carbon\CarbonImmutable;

class StockIncreased extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var StockId
     */
    private $stockId;

    /**
     * @var int
     */
    private $qty;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var CarbonImmutable
     */
    private $date;

    public function __construct(ProductId $productId, StockId $stockId, float $cost, int $qty, CarbonImmutable $date)
    {
        parent::__construct();

        $this->id = $productId;
        $this->stockId = $stockId;
        $this->cost = $cost;
        $this->qty = $qty;
        $this->date = $date;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function stockId(): StockId
    {
        return $this->stockId;
    }

    public function cost(): float
    {
        return $this->cost;
    }

    public function qty(): int
    {
        return $this->qty;
    }

    public function date(): CarbonImmutable
    {
        return $this->date;
    }
}
