<?php

namespace Captainskippah\Inventory\Domain\Product;

interface ProductRepository
{
    public function getById(ProductId $productId): ?Product;

    public function save(Product $product);

    public function delete(ProductId $productId);

    public function nextIdentity(): ProductId;
}
