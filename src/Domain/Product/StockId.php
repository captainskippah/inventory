<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\AbstractId;

class StockId extends AbstractId
{
    public function __construct(string $stockId)
    {
        parent::__construct($stockId);
    }
}
