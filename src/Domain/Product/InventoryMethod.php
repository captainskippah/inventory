<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;

interface InventoryMethod
{
    public function takeNextStock(CarbonImmutable $date, AbstractStock ...$stocks): ?AbstractStock;
}
