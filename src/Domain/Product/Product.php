<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\EventSourcedAggregateRoot;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;

class Product extends EventSourcedAggregateRoot
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $depletingQty = 0;

    /**
     * @var AbstractStock[]
     */
    private $stocks = [];

    public function __construct(ProductId $id, string $name, int $depletingQty = 0, Stock ...$stocks)
    {
        $this->apply(new ProductCreated($id, $name, $depletingQty, ...$stocks));
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function depletingQty(): int
    {
        return $this->depletingQty;
    }

    /**
     * @return AbstractStock[]
     */
    public function stocks(): array
    {
        return $this->stocks;
    }

    public function stockQty(): int
    {
        return array_reduce($this->stocks(), function (int $prev, Stock $stock) {
            return $prev + $stock->qty();
        }, 0);
    }

    public function costOfGoodsAvailable(): float
    {
        return array_reduce($this->stocks(), function (int $prev, Stock $stock) {
            return $prev + ($stock->cost() * $stock->qty());
        }, 0);
    }

    public function rename(string $name)
    {
        $this->apply(new ProductRenamed($this->id(), $name));
    }

    public function updateDepletingQty(int $depletingQty)
    {
        $this->apply(new ProductDepletingQtyUpdated($this->id(), $depletingQty));
    }

    public function restock(float $cost, int $qty, CarbonImmutable $date = null)
    {
        $date = $date ?? new CarbonImmutable();

        $missingStock = $this->getMissingStock($date);

        if ($missingStock !== null) {
            $remaining = $qty;

            // Incoming restock qty is not enough to resolve MissingStock
            // Recorded restock will have 0 qty right away
            if ($missingStock->qty() > $remaining) {
                $this->apply(new MissingStockDecreased($this->id(), $missingStock->id(), $remaining, $date));

                $stockId = new StockId(Uuid::uuid4()->serialize());

                $this->apply(new StockIncreased($this->id(), $stockId, $cost, 0, $date));
            } else {
                $remaining -= $missingStock->qty();

                $this->apply(new MissingStockDecreased($this->id(), $missingStock->id(), $missingStock->qty(), $date));

                $stockId = new StockId(Uuid::uuid4()->serialize());

                $this->apply(new StockIncreased($this->id(), $stockId, $cost, $remaining, $date));
            }
        } else {
            $stock = $this->getStock($date, $cost);

            $stockId = $stock !== null
                ? $stock->id()
                : new StockId(Uuid::uuid4()->serialize());

            $this->apply(new StockIncreased($this->id(), $stockId, $cost, $qty, $date));
        }
    }

    public function take(InventoryMethod $method, int $qty, CarbonImmutable $date = null)
    {
        $remaining = $qty;
        $date = $date ?? new CarbonImmutable();

        while ($remaining > 0) {
            $stock = $method->takeNextStock($date, ...$this->stocks());

            if ($stock === null) {
                $missingStock = $this->getMissingStock($date);

                $missingStockId = $missingStock !== null
                    ? $missingStock->id()
                    : new MissingStockId(Uuid::uuid4()->serialize());


                $this->apply(new MissingStockIncreased($this->id(), $missingStockId, $remaining, $date));
                break;
            }

            if ($stock->qty() < $remaining) {
                $remaining -= $stock->qty();
                $this->apply(new StockDecreased($this->id(), $stock->id(), $stock->qty(), $date));
            } else {
                $this->apply(new StockDecreased($this->id(), $stock->id(), $remaining, $date));
                break;
            }
        }
    }

    public function amendStockCost(StockId $stockId, float $cost)
    {
        if (!$this->stockIdExists($stockId)) {
            throw new \Exception(sprintf('Product of ID %s does not contain stock of ID %s', $this->id()->value(), $stockId->value()));
        }

        $this->apply(new StockCostAmended($this->id(), $stockId, $cost));
    }

    protected function whenProductCreated(ProductCreated $event)
    {
        $this->id = $event->id();
        $this->name = $event->name();
        $this->depletingQty = $event->depletingQty();
        $this->stocks = $event->stocks();
    }

    protected function whenProductRenamed(ProductRenamed $event)
    {
        $this->name = $event->name();
    }

    protected function whenProductDepletingQtyUpdated(ProductDepletingQtyUpdated $event)
    {
        $this->depletingQty = $event->depletingQty();
    }

    protected function whenStockIncreased(StockIncreased $event)
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($event->stockId())) {
                $stock->increase($event->qty());
                return;
            }
        }

        $this->stocks[] = new Stock($event->stockId(), $event->cost(), $event->qty(), $event->date());
    }

    protected function whenStockDecreased(StockDecreased $event)
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($event->stockId())) {
                $stock->decrease($event->qty());
                break;
            }
        }
    }

    protected function whenMissingStockIncreased(MissingStockIncreased $event)
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($event->stockId())) {
                $stock->increase($event->qty());
                return;
            }
        }

        $this->stocks[] = new MissingStock($event->stockId(), $event->qty(), $event->date());
    }

    protected function whenMissingStockDecreased(MissingStockDecreased $event)
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($event->stockId())) {
                $stock->decrease($event->qty());
                break;
            }
        }
    }

    protected function whenStockCostAmended(StockCostAmended $event)
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($event->stockId())) {
                $stock->amendCost($event->cost());
                break;
            }
        }
    }

    private function stockIdExists(StockId $stockId): bool
    {
        foreach ($this->stocks() as $stock) {
            if ($stock->id()->equals($stockId)) {
                return true;
            }
        }

        return false;
    }

    private function getMissingStock(CarbonImmutable $date): ?MissingStock
    {
        foreach ($this->stocks() as $stock) {
            if (!$stock instanceof MissingStock) {
                continue;
            }

            if ($stock->date()->isSameDay($date)) {
                return $stock;
            }
        }

        return null;
    }

    private function getStock(CarbonImmutable $date, float $cost): ?Stock
    {
        foreach ($this->stocks() as $stock) {
            if (!$stock instanceof Stock) {
                continue;
            }

            if ($stock->date()->isSameDay($date) && $stock->cost() === $cost) {
                return $stock;
            }
        }

        return null;
    }
}
