<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;

class StockCostAmended extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var StockId
     */
    private $stockId;

    /**
     * @var float
     */
    private $cost;

    public function __construct(ProductId $productId, StockId $stockId, float $cost)
    {
        parent::__construct();

        $this->id = $productId;
        $this->stockId = $stockId;
        $this->cost = $cost;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function stockId(): StockId
    {
        return $this->stockId;
    }

    public function cost(): float
    {
        return $this->cost;
    }
}
