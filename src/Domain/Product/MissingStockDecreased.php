<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;
use Carbon\CarbonImmutable;

class MissingStockDecreased extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var MissingStockId
     */
    private $stockId;

    /**
     * @var int
     */
    private $qty;

    /**
     * @var CarbonImmutable
     */
    private $date;

    public function __construct(ProductId $id, MissingStockId $stockId, int $qty, CarbonImmutable $date)
    {
        parent::__construct();

        $this->id = $id;
        $this->stockId = $stockId;
        $this->qty = $qty;
        $this->date = $date;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function stockId(): MissingStockId
    {
        return $this->stockId;
    }

    public function qty(): int
    {
        return $this->qty;
    }

    public function date(): CarbonImmutable
    {
        return $this->date;
    }
}
