<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;

class ProductCreated extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $depletingQty;

    /**
     * @var Stock[]
     */
    private $stocks;

    public function __construct(ProductId $productId, string $name, int $depletingQty, Stock ...$stocks)
    {
        parent::__construct();

        $this->id = $productId;
        $this->name = $name;
        $this->depletingQty = $depletingQty;
        $this->stocks = $stocks;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function depletingQty(): int
    {
        return $this->depletingQty;
    }

    /**
     * @return Stock[]
     */
    public function stocks(): array
    {
        return $this->stocks;
    }
}
