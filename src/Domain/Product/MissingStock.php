<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;

class MissingStock extends AbstractStock
{
    /**
     * @var MissingStockId
     */
    private $id;

    public function __construct(MissingStockId $id, int $qty, CarbonImmutable $date)
    {
        parent::__construct($qty, $date);
        $this->id = $id;
    }

    public function id(): MissingStockId
    {
        return $this->id;
    }

    public function increase(int $qty)
    {
        $this->qty += $qty;
    }

    public function decrease(int $qty)
    {
        if ($qty > $this->qty()) {
            $excess = $qty - $this->qty();
            throw new \RuntimeException("Cannot decrease more than $this->qty. Quantity of $excess should be added as new Stock.");
        }

        $this->qty -= $qty;
    }
}
