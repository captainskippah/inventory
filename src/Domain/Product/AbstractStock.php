<?php

namespace Captainskippah\Inventory\Domain\Product;

use Carbon\CarbonImmutable;

abstract class AbstractStock
{
    /**
     * @var int
     */
    protected $qty;

    /**
     * @var CarbonImmutable
     */
    protected $date;

    protected function __construct(int $qty, CarbonImmutable $date)
    {
        $this->qty = $qty;
        $this->date = $date;
    }

    public function qty(): int
    {
        return $this->qty;
    }

    public function date(): CarbonImmutable
    {
        return $this->date;
    }

    public function amendQty(int $qty)
    {
        $this->qty = $qty;
    }

    abstract public function increase(int $qty);

    abstract public function decrease(int $qty);
}
