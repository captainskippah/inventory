<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;

class ProductDepletingQtyUpdated extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var int
     */
    private $depletingQty;

    public function __construct(ProductId $productId, int $depletingQty)
    {
        parent::__construct();

        $this->id = $productId;
        $this->depletingQty = $depletingQty;
    }

    /**
     * @return ProductId
     */
    public function id(): ProductId
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function depletingQty(): int
    {
        return $this->depletingQty;
    }
}
