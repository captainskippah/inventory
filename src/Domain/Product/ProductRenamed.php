<?php

namespace Captainskippah\Inventory\Domain\Product;

use Captainskippah\Common\Domain\DomainEvent;

class ProductRenamed extends DomainEvent
{
    /**
     * @var ProductId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    public function __construct(ProductId $productId, string $name)
    {
        parent::__construct();

        $this->id = $productId;
        $this->name = $name;
    }

    /**
     * @return ProductId
     */
    public function id(): ProductId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
