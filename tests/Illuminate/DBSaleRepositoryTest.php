<?php

namespace Captainskippah\Inventory\Tests\Illuminate;

use Captainskippah\Common\Illuminate\Event\EventStoreServiceProvider;
use Captainskippah\Common\Illuminate\Serializer\SerializerServiceProvider;
use Captainskippah\Inventory\Domain\Product\ProductId;
use Captainskippah\Inventory\Domain\Sales\Sale;
use Captainskippah\Inventory\Domain\Sales\SaleItem;
use Captainskippah\Inventory\Domain\Sales\SaleRepository;
use Captainskippah\Inventory\Infrastructure\Illuminate\InventoryServiceProvider;
use Captainskippah\Inventory\Infrastructure\Illuminate\Repositories\DBSaleRepository;
use Carbon\CarbonImmutable;
use Orchestra\Testbench\TestCase;

class DBSaleRepositoryTest extends TestCase
{
    /**
     * @var SaleRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('vendor:publish', ['--provider' => InventoryServiceProvider::class])->run();
        $this->artisan('migrate', ['--database' => 'testing'])->run();

        $this->repository = new DBSaleRepository($this->app->make('db.connection'));
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default.', 'testing');
    }

    protected function getPackageProviders($app)
    {
        return [
            SerializerServiceProvider::class,
            EventStoreServiceProvider::class,
        ];
    }

    public function testRepository()
    {
        // Arrange
        $items = [
            new SaleItem(new ProductId('something-1'), 10, 100),
            new SaleItem(new ProductId('something-2'), 5, 200)
        ];

        $sale = new Sale($this->repository->nextIdentity(), new CarbonImmutable(), ...$items);

        // Act
        $this->repository->save($sale);

        // Assert
        $saved = $this->repository->getById($sale->id());

        self::assertTrue($sale->date()->isSameDay($saved->date()));
        self::assertCount(2, $saved->items());
    }
}
