<?php

namespace Captainskippah\Inventory\Tests\Illuminate;

use Captainskippah\Common\Illuminate\Event\EventStoreServiceProvider;
use Captainskippah\Common\Illuminate\Serializer\SerializerServiceProvider;
use Captainskippah\Inventory\Domain\Product\FIFOMethod;
use Captainskippah\Inventory\Domain\Product\Product;
use Captainskippah\Inventory\Domain\Product\ProductRepository;
use Captainskippah\Inventory\Domain\Product\Stock;
use Captainskippah\Inventory\Domain\Product\StockId;
use Captainskippah\Inventory\Infrastructure\EventStoreProductRepository;
use Carbon\CarbonImmutable;
use Orchestra\Testbench\TestCase;
use Ramsey\Uuid\Uuid;

class EventStoreProductRepositoryTest extends TestCase
{
    /**
     * @var ProductRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('vendor:publish', ['--provider' => EventStoreServiceProvider::class])->run();
        $this->artisan('migrate', ['--database' => 'testing'])->run();

        $this->app->bind(ProductRepository::class, EventStoreProductRepository::class);
        $this->repository = $this->app->make(ProductRepository::class);
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default.', 'testing');
    }

    protected function getPackageProviders($app)
    {
        return [
            SerializerServiceProvider::class,
            EventStoreServiceProvider::class
        ];
    }

    public function testShouldPersist()
    {
        // Arrange
        $stocks = [
            new Stock(
                new StockId(Uuid::uuid4()->serialize()),
                1000,
                24,
                CarbonImmutable::create(2019, 1, 1)
            )
        ];

        $product = new Product($this->repository->nextIdentity(), 'Test Product', 0, ...$stocks);

        // Act
        $this->repository->save($product);

        // Assert: should clear events
        self::assertCount(0, $product->events());

        // Assert: version 1
        $product = $this->repository->getById($product->id());

        self::assertEquals(1, $product->unmutatedVersion());
        self::assertCount(1, $product->stocks());

        // Act
        $product->take(new FIFOMethod(), 12, CarbonImmutable::create(2019, 1, 4));

        $this->repository->save($product);

        // Assert: version 2
        $product = $this->repository->getById($product->id());

        self::assertEquals(2, $product->unmutatedVersion());

        // Act
        $product->restock(1020, 12, CarbonImmutable::create(2019, 1, 7));

        $this->repository->save($product);

        // Assert: version 3
        $product = $this->repository->getById($product->id());

        self::assertEquals(3, $product->unmutatedVersion());
    }
}
