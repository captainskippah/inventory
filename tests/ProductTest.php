<?php

namespace Captainskippah\Inventory\Tests;

use Captainskippah\Inventory\Domain\Product\FIFOMethod;
use Captainskippah\Inventory\Domain\Product\LIFOMethod;
use Captainskippah\Inventory\Domain\Product\MissingStock;
use Captainskippah\Inventory\Domain\Product\Product;
use Captainskippah\Inventory\Domain\Product\ProductId;
use Captainskippah\Inventory\Domain\Product\Stock;
use Captainskippah\Inventory\Domain\Product\StockId;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProductTest extends TestCase
{
    public function testShouldRenameProduct()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        self::assertEquals("Awesome Product", $product->name());

        // Act
        $product->rename("Product Name 2");

        // Assert
        self::assertEquals("Product Name 2", $product->name());
        self::assertCount(2, $product->events());
    }

    public function testShouldUpdateDepletingQty()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product", 20);

        self::assertEquals(20, $product->depletingQty());

        // Act
        $product->updateDepletingQty(10);

        // Assert
        self::assertEquals(10, $product->depletingQty());
        self::assertCount(2, $product->events());
    }

    public function testShouldRestockProduct()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        // Act
        $product->restock(100, 20);
        $product->restock(100, 10);

        // Assert
        self::assertEquals(30, $product->stockQty());
        self::assertCount(3, $product->events());
    }

    public function testShouldHaveOneStockForGivenDateAndCost()
    {
        // Arrange
        $stocks = [
            new Stock(new StockId(Uuid::uuid4()->serialize()), 1000, 5, CarbonImmutable::create(2019, 1, 5)),
            new Stock(new StockId(Uuid::uuid4()->serialize()), 2000, 10, CarbonImmutable::create(2019, 1, 10))
        ];

        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product", 0, ...$stocks);

        // Act
        $product->restock(1000, 10, CarbonImmutable::create(2019, 1, 5));

        // Assert
        self::assertCount(2, $product->stocks());
        self::assertEquals(15, $product->stocks()[0]->qty());
    }

    public function testShouldTakeFromOldestStock()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->restock(50, 5);
        $product->restock(100, 10);
        $product->restock(200, 7);

        // Act
        $product->take(new FIFOMethod(), 10);

        // Assert
        self::assertEquals(0, $product->stocks()[0]->qty());
        self::assertEquals(5, $product->stocks()[1]->qty());
        self::assertEquals(7, $product->stocks()[2]->qty());

        self::assertCount(6, $product->events());
    }

    public function testShouldTakeFromLatestStock()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->restock(50, 5);
        $product->restock(100, 10);
        $product->restock(200, 7);

        // Act
        $product->take(new LIFOMethod(), 10);

        // Assert
        self::assertEquals(5, $product->stocks()[0]->qty());
        self::assertEquals(7, $product->stocks()[1]->qty());
        self::assertEquals(0, $product->stocks()[2]->qty());

        self::assertCount(6, $product->events());
    }

    public function testShouldNotTakeStocksFromFuture()
    {
        // Arrange
        $stock = [
            new Stock(new StockId(Uuid::uuid4()->serialize()), 100, 10, CarbonImmutable::create(2019, 1, 1)),
            new Stock(new StockId(Uuid::uuid4()->serialize()), 150, 8, CarbonImmutable::create(2019, 1, 1))
        ];

        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product", 0, ...$stock);

        $product->restock(200, 6, CarbonImmutable::create(2019, 1, 2));

        // Act
        $product->take(new LIFOMethod(), 10, CarbonImmutable::create(2019, 1, 1));

        // Assert
        self::assertEquals(8, $product->stocks()[0]->qty());
        self::assertEquals(0, $product->stocks()[1]->qty());
        self::assertEquals(6, $product->stocks()[2]->qty());

        self::assertCount(4, $product->events());
    }

    public function testShouldComputeTotalStocksQuantity()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->restock(100, 10);
        $product->take(new FIFOMethod(), 5);
        $product->restock(90, 15);
        $product->take(new LIFOMethod(), 10);

        // Assert
        self::assertEquals(10, $product->stockQty());
    }

    public function testShouldComputeCostOfGoodsAvailable()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->restock(100, 10);
        $product->take(new FIFOMethod(), 5);
        $product->restock(90, 15);
        $product->take(new LIFOMethod(), 10);

        // Assert
        self::assertEquals(950, $product->costOfGoodsAvailable());
    }

    public function testShouldAmendStockCost()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->restock(50, 10);
        $product->restock(100, 10);

        // Amend this wrong Cost stock
        $product->restock(1, 10);

        // Act
        $stockId = $product->events()[3]->stockId();

        $product->amendStockCost($stockId, 110);

        // Assert
        self::assertEquals(2600, $product->costOfGoodsAvailable());
    }

    public function testShouldCreateMissingStockWhenTakingWithNoStocks()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        // Act
        $product->take(new FIFOMethod(), 10);

        // Assert
        self::assertCount(1, $product->stocks());
        self::assertInstanceOf(MissingStock::class, $product->stocks()[0]);
        self::assertEquals(10, $product->stocks()[0]->qty());

        // Act
        $product->take(new FIFOMethod(), 5);

        // Assert
        self::assertCount(1, $product->stocks());
        self::assertInstanceOf(MissingStock::class, $product->stocks()[0]);
        self::assertEquals(15, $product->stocks()[0]->qty());
    }

    public function testShouldCreateMissingStockWhenTakingWithoutSufficientStocks()
    {
        // Arrange
        $stocks = [
            new Stock(new StockId(Uuid::uuid4()->serialize()), 1000, 8, new CarbonImmutable())
        ];

        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product", 0, ...$stocks);

        // Act
        $product->take(new FIFOMethod(), 16);

        // Assert
        self::assertCount(2, $product->stocks());

        self::assertInstanceOf(Stock::class, $product->stocks()[0]);
        self::assertEquals(0, $product->stocks()[0]->qty());

        self::assertInstanceOf(MissingStock::class, $product->stocks()[1]);
        self::assertEquals(8, $product->stocks()[1]->qty());
    }

    public function testShouldHaveOneMissingStockOnlyForGivenDate()
    {
        // Arrange
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");

        $product->take(new FIFOMethod(), 10, $date1 = CarbonImmutable::create(2019, 1, 5));
        $product->take(new FIFOMethod(), 10, $date2 = CarbonImmutable::create(2019, 1, 10));

        // Act
        $product->take(new FIFOMethod(), 15, CarbonImmutable::create(2019, 1, 10));
        $product->take(new FIFOMethod(), 10, CarbonImmutable::create(2019, 1, 5));

        // Assert
        self::assertEquals(20, $product->stocks()[0]->qty());
        self::assertTrue($product->stocks()[0]->date()->eq($date1));

        self::assertEquals(25, $product->stocks()[1]->qty());
        self::assertTrue($product->stocks()[1]->date()->eq($date2));
    }

    public function testRestockShouldDecreaseMissingStock()
    {
        // Arrange: Create 10 MissingStock
        $product = new Product(new ProductId("awesomeProduct"), "Awesome Product");
        $product->take(new FIFOMethod(), 10);

        // Act
        $product->restock(10, 5);

        // Assert: should still have 5 MissingStock left and 0 available
        self::assertInstanceOf(MissingStock::class, $product->stocks()[0]);
        self::assertEquals(5, $product->stocks()[0]->qty());
        self::assertEquals(0, $product->stocks()[1]->qty());

        // Act
        $product->restock(10, 10);

        // Assert: should have 0 MissingStock left and 5 available
        self::assertEquals(0, $product->stocks()[0]->qty());
        self::assertEquals(5, $product->stocks()[2]->qty());

        // Arrange: Create 5 MissingStock
        $product->take(new FIFOMethod(), 10);

        // Act
        $product->restock(20, 20);

        // Assert: should have 0 MissingStock left and 15 available
        self::assertInstanceOf(MissingStock::class, $product->stocks()[0]);
        self::assertEquals(0, $product->stocks()[0]->qty());
        self::assertEquals(15, $product->stocks()[3]->qty());
    }
}
